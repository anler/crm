# API Test - The CRM Service

REST API to manage customer data for a small shop. It works as the
backend side for a CRM interface that is being developed by a
different team.

## Running

To run the program execute:

``` shell
cargo run
```

And execute the following to display the program help:

``` shell
cargo run -- --help
```

