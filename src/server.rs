use std::net;

use actix_web::{web, App, HttpRequest, HttpServer, Responder};

fn greet(req: HttpRequest) -> impl Responder {
    let name = req.match_info().get("name").unwrap_or("World");
    format!("Hello {}!", &name)
}

pub fn run(addr: (net::IpAddr, u16)) -> Result<(), failure::Error> {
    HttpServer::new(|| {
        App::new()
            .route("/", web::get().to(greet))
            .route("/{name}", web::get().to(greet))
    })
    .bind(addr)
    .map_err(|e| {
        log::error!("{}", e);
        failure::err_msg(format!("Failed to bind to {}:{}. {}", addr.0, addr.1, e))
    })?
    .run()?;

    Ok(())
}
