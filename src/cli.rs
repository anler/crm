use std::ffi::OsString;
use std::net;
use std::str::FromStr as _;

pub struct CliParams {
    pub addr: (net::IpAddr, u16),
    pub log_level: log::Level,
}

pub fn parse_params<I, T>(args: I) -> Result<CliParams, failure::Error>
where
    I: IntoIterator<Item = T>,
    T: Into<OsString> + Clone,
{
    let matches = app_from_crate!()
        .arg(
            clap::Arg::with_name("LOG_LEVEL")
                .long("log-level")
                .help("The level to use when logging.")
                .default_value("info"),
        )
        .arg(
            clap::Arg::with_name("PORT")
                .short("p")
                .long("port")
                .help("Http server port.")
                .takes_value(true)
                .default_value("8000"),
        )
        .arg(
            clap::Arg::with_name("HOST")
                .short("h")
                .long("host")
                .help("Http server host.")
                .takes_value(true)
                .default_value("127.0.0.1"),
        )
        .get_matches_from(args);

    let log_level = log::Level::from_str(
        matches
            .value_of("LOG_LEVEL")
            .expect("no default value for log level"),
    )
    .map_err(|_| {
        failure::err_msg("Unrecognized log level. Must be: error | warn | info | debug | trace")
    })?;
    let host = net::IpAddr::from_str(matches.value_of("HOST").expect("no default value for host"))
        .map_err(|_| failure::err_msg("Invalid host."))?;
    let port = u16::from_str(matches.value_of("PORT").expect("no default value for port"))
        .map_err(|_| failure::err_msg("Invalid port."))?;

    Ok(CliParams {
        addr: (host, port),
        log_level,
    })
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_cli_default_values() {
        let args = Vec::<String>::new();
        let params = parse_params(args).unwrap();

        assert_eq!(log::Level::Info, params.log_level);
        assert_eq!(
            (net::IpAddr::V4(net::Ipv4Addr::new(127, 0, 0, 1)), 8000u16),
            params.addr
        );
    }

    #[test]
    fn test_cli_host_value() {
        let args = vec!["crm", "-h", "168.1.1.0"];
        let params = parse_params(args).unwrap();

        assert_eq!(
            (net::IpAddr::V4(net::Ipv4Addr::new(168, 1, 1, 0)), 8000u16),
            params.addr
        );
    }

    #[test]
    fn test_cli_port_value() {
        let args = vec!["crm", "-p", "3000"];
        let params = parse_params(args).unwrap();

        assert_eq!(
            (net::IpAddr::V4(net::Ipv4Addr::new(127, 0, 0, 1)), 3000u16),
            params.addr
        );
    }
}
