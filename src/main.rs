use std::env;
use std::process;

#[macro_use]
extern crate clap;

mod cli;
mod server;

fn main() {
    if let Err(e) = cli::parse_params(env::args()).and_then(|params| {
        simple_logger::init_with_level(params.log_level)
            .map_err(|e| failure::err_msg(format!("Failed to configure logger: {}", e)))?;
        server::run(params.addr)
    }) {
        eprintln!("{}", e);
        process::exit(1);
    }
}
